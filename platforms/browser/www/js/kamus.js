String.prototype.replaceAll = function(s,r){return this.split(s).join(r)}

angular.module('DoEventApp', []).controller('MainCtrl',function($scope, $http, $sce){
    $scope.input_huruf = "";
    $scope.hasil_hana = "";
    $scope.$watch("input_huruf",
        function( newValue, oldValue ) {
            if(newValue!=oldValue){
                $scope.hasil_hana = $scope.input_huruf;
            }
        }
    );
});
angular.bootstrap(document.body, ['DoEventApp']);